
-- Adams add and double number function

add_and_double ::(Num a) => a -> a -> a
--add_and_double []_= []
add_and_double x y = (x + y) * 2; 

-- Adams infix function 
add_and_double' x y = x `add_and_double` y 

--Adams solve quardratic function
solve_quadratic_equation :: (Double,Double,Double) -> (Double,Double) 
solve_quadratic_equation (a,b,c) = let d = sqrt(b^2-4*a*c) in ((-b+d)/(2*a),(-b-d)/(2*a)) 


--Adams first_n function
first_n :: Int -> [Int] -> [Int]
first_n  a (x:xs)= x: take (a-1) xs  

--Adams take_integer function
--first_integer :: Integer -> [Integer] -> [Integer]
--take_integer:: Integer -> [Integer] -> [Integer]


--Adams Double_factorial function
double_factorial :: Integer -> Integer
double_factorial 0 = 1 
double_factorial a = (a * double_factorial (a - 1))


--Adams isPrime Function
-- isPrime:: Integer -> Bool 
-- isPrime a =
     -- if a / not a =1 
              -- then return False
-- else return True


--Adams Sum Integers Function
sum_integers :: Integer -> Integer -> Integer
sum_integers x y = sum[x..y]

--Adams foldl sum Function
sum_integers_foldl :: Integer -> Integer -> Integer
sum_integers_foldl x y = foldl(+)0[x..y]

--Adams foldr multipy Function
mult_integer :: Integer -> Integer -> Integer
mult_integer x y = foldr(*)1[x..y]



-- Adams guess function 
guess :: String -> Int -> String
sentence = "I love functional programming"
wrong_sentence = ""
number= 5
guess sentence number = if sentence == sentence && number == 5 
                             then "Congrats you win Lad"
							 else if sentence /= sentence && number < 5
							 then "Guess Again ya Chancer"
							 else if sentence /= sentence && number > 5 
							 then "Go home you lost ... Loser!"
	

--Adams is_even function
-- Assisted by Lukas Petraitis
-- Originally Used the ("/") symbol at first then with help from lukas had to use `mod`
is_even :: Int -> Bool 
is_even n 
     | n `mod` 2 == 0 = True 
     |otherwise = False 
	 
	 --Adams Unixname function
unixname :: String -> String
unixname = filter (`notElem` "aeiou")
 

--Adams intersection function
--intersection :: [Integer] -> [Integer]
--intersection (x:xs) 

--Adams dot Product function 
dotProduct :: (Num a) => [a] -> [a] -> a
dotProduct [] _ = 0
dotProduct _ [] = 0
dotProduct (x:xs) (y:ys) = (x * y) + dotProduct xs ys
